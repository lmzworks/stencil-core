package works.lmz.stencil

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Author: Richard Vowles - http://gplus.to/RichardVowles
 *
 * If this annotation exists (its a Highlander Annotation - There Can Be Only One)
 * then this becomes the page that the StencilDispatchFilter redirects to
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DefaultStencil {
}